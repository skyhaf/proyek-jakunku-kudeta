from django.urls import path
from . import views

app_name = 'trialerrordifoldersiniaja'

urlpatterns = [
    path('', views.trial, name="trial"),
    path('user/',views.user, name="user"),
    path('user/<username>',views.user_profile),
    path('user/<username>/edit',views.edit_profile),
]