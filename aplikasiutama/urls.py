from django.urls import path
from . import views

app_name = 'aplikasiutama'

urlpatterns = [
    path('', views.landing, name="landing"),
    path('login/', views.login, name="login"),
    path('logout/', views.logout, name="logout"),
]