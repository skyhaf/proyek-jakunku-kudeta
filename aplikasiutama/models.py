from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Mahasiswa(models.Model):
    # img = models.ImageField(upload_to=,default=)
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    fakultas = models.CharField(max_length=50)
    angkatan = models.CharField(max_length=3)
    size_punya = models.CharField(max_length=5)
    size_ingin = models.CharField(max_length=5)
    desc = models.TextField()
    sudah_tukar = models.BooleanField(default=False)

    def __str__(self):
        return self.user.__str__()